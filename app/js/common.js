$(window).on('load', function() {
	$('.preloader').delay(500).fadeOut('slow');
});

$(window).scroll(function() {
	if ($(this).scrollTop() > $(this).height()) {
		$('.scroll-top').addClass('active');
	} else {
		$('.scroll-top').removeClass('active');
	}
});

$(function() {

	/**
	* Short Scripts
	*/
	$(".s-about .h4").equalHeights();

	/**
	* Navigation Desktop & Mobile
	*/
	$("ul.sf-menu").clone().appendTo("#my-menu div");
	$("#my-menu div").find("*").attr("style", "");
	$("#my-menu div").find("ul").removeClass("sf-menu");
	var m = $("#my-menu").mmenu({
		extensions: {
			all: ["theme-grey", "fx-menu-slide", "pagedim-black"]
		},
		navbar: {
			title: ""
		},
		pageScroll : {
			scroll : true,
			update : true,
			scrollOffset : 50
		},
		classNames: {
			selected: "active"
		},
		offCanvas: {
			position: 'left'
		}
	}).data("mmenu");;
	
	$("#mobile-menu").click(function() {
		$('html').hasClass("mm-opened") ? m.close() : m.open();
		m.bind("open:start", function() {
			$(".hamburger").addClass("is-active");
		}).bind("close:start", function() {
			$(".hamburger").removeClass("is-active");
		});
		return false;
	});

	/**
	* Popup CallBack
	*/

	var newBlockName = "callback-popup",
	originalBlockName = "callback";

	if(!$("#"+newBlockName).length) {
		$("#"+originalBlockName).clone()
		.addClass("popup-box zoom-anim-dialog")
		.removeAttr("id")
		.attr("id", newBlockName)
		.appendTo("#hidden")
		.find("label.textarea").remove(); // Change it
	}

	$("a[href='#"+newBlockName+"']").magnificPopup({
		type: 'inline',

		fixedContentPos: true,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});

	/**
	* Close Button
	*/
	$(".popup-box .close").click(function() {
		$.magnificPopup.close();
	});

	/**
	* Here you can put your own events on click.
	*/
	$("a[href='#"+newBlockName+"']").click(function() {
		var formName = $(this).data("name");
		if ( formName !== undefined && formName !== false) {
			$('#'+newBlockName+' input[name="Форма"]').attr("value", formName);
		} else {
			$('#'+newBlockName+' input[name="Форма"]').attr("value", "Данные не указаны");
		}
		return false;
	});

	/**
	* Masks For Form Fields
	*/
		$(".callback input[type='tel']").inputmask("+7 (999) 999-99-99");
		// $('.callback select').selectize();

	/**
	* E-mail Ajax Send
	*/
	$("form").submit(function() {
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php",
			data: th.serialize()
		}).done(function() {
			th.trigger("reset");
			$.magnificPopup.open({
				items: {
					src: $("#callback-success-popup")
				},
				type: 'inline',

				fixedContentPos: true,
				fixedBgPos: true,

				overflowY: 'auto',

				closeBtnInside: true,
				preloader: false,
				
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		});
		return false;
	});

	/**
	* Portfolio Gallery
	*/
	$(".s-portfolio .portfolio-item").each(function(index) {
		var th 		= $(this),
		id 		= 'portfolio-item-gallery-' + index,
		href 	= '#'+id
		th.find('.portfolio-item-image').attr('href', href )
		th.find('.hidden').attr( 'id', id )

		$("a[href='"+href+"']").click(function() {
			$(this).photoSwipe();
			return false;
		});

		var review_open = th.find(".review-open"),
		review_num = review_open.data("review");
		if(review_num !== undefined && review_num !== false) {

			var newBlockName = "review-popup-"+review_num,
			originalBlockName = "review-"+review_num;
			review_open.attr('href', "#"+newBlockName)

			if(!$("#"+newBlockName).length) {
				$("#"+originalBlockName).clone()
				.addClass("popup-box zoom-anim-dialog")
				.removeAttr("id")
				.attr("id", newBlockName)
				.appendTo("#hidden");
			}

			review_open.magnificPopup({
				type: 'inline',

				fixedContentPos: true,
				fixedBgPos: true,

				overflowY: 'auto',

				closeBtnInside: true,
				preloader: false,

				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		};

	});

	if ($(window).width() > 767) {
		$(".s-portfolio .portfolio-item").each(function() {
			var th 		= $(this);
			th.find('.portfolio-item-browser').css('height', th.height());	
		});
	}

	/**
	* Reviews Carousel
	*/
	$('.carousel-reviews').owlCarousel({
		loop: true,
		nav: true,
		dots: true,
		autoHeight: true,
		smartSpeed: 700,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		items: 1
	});

	/**
	 * Animation
	 */
	 var t = $("#typist");
	 t.typist({
	 	speed: 12,
	 	text: t.data("text")
	 })
	 .typistPause(t.data("pause"))
	 .typistRemove(t.data("remove"))
	 .typistAdd(t.data("add"))
	 .typistStop();

	/**
	 * Scroll Top Button Script
	 */
	 $('.scroll-top').click(function() {
	 	$('html, body').stop().animate({scrollTop: 0}, 'slow', 'swing');
	 });

	/**
	 * Laze load images & iframes
	 */
	var iframe_LazyLoad = new LazyLoad({
		elements_selector: "iframe, .review-photo-inner",
		threshold: 600
	});

	var myLazyLoad = new LazyLoad({
		elements_selector: ".portfolio-item-image, .skills-item, img",
		threshold: 500
	});

	$(".review-open").click(function() {
		iframe_LazyLoad.update();
	});

});
