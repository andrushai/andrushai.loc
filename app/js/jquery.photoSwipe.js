/**
 * jQuery Photoswipe http://photoswipe.com/
 * jQuery plugin for photoswipe
 * Original: https://github.com/dimsemenov/photoswipe
 * Plugin author: andrushai
 */
(function($) {
	$.fn.photoSwipe = function() {

	/**
	 * Get array of images from block where href linked
	 */
	 var contnet = $(this).attr("href");
	 getItems = function() {
	 	var items = [];
	 	$(contnet).find('a').each(function() {
	 		var href   = $(this).attr('href'),
	 		width  = $(this).data('width')
	 		height = $(this).data('height')
	 		var item = {
	 			src : href,
	 			w   : width,
	 			h   : height
	 		}
	 		items.push(item);
	 	});
	 	return items;
	 };
	 var items = getItems();

	/**
	 * Defauot Options
	 */
	 var defaults = {
	 	itemSelector: 'a',
	 	bgOpacity : .95,
	 	loop : 'true',
	 	closeOnScroll : false,
	 	escKey: true,
	 	history: false,
		//mainClass : 'pswp--minimal--dark',
		barsSize : {top:50,bottom:20},
		tapToClose : false,
		tapToToggleControls : true,
		showHideOpacity: true,
		//Button
		closeEl:true,
		captionEl: true,
		zoomEl: true,
		shareEl: true,
		counterEl: true,
		arrowEl: true,
		preloaderEl: true,
		shareButtons: [
		{id:'facebook', label:'Share on Facebook', url:'https://www.facebook.com/sharer/sharer.php?u={{url}}'},
		{id:'twitter', label:'Tweet', url:'https://twitter.com/intent/tweet?text={{text}}&url={{url}}'},
		{id:'pinterest', label:'Pin it', url:'http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}'},
		{id:'download', label:'Download image', url:'{{raw_image_url}}', download:true}
		],
	}

	/**
	 *  Create Photoswipe element if not exist
	 *  important, this element should exist if you call photoswipe instance
	 */
	 var createDialogElement = function(){
	 	$('body').find('.pswp').remove();
	 	if($('body').find('.pswp').length == 0) {
	 		$('body').prepend(photoSwipeTemplate);
	 	}
	 }

	 var photoSwipeTemplate = ' <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">\
	 <div class="pswp__bg"></div> \
	 <div class="pswp__scroll-wrap"> \
	 <div class="pswp__container"> \
	 <div class="pswp__item"></div> \
	 <div class="pswp__item"></div> \
	 <div class="pswp__item"></div> \
	 </div>\
	 <div class="pswp__ui pswp__ui--hidden"> \
	 <div class="pswp__top-bar"> \
	 <div class="pswp__counter"></div> \
	 <button class="pswp__button pswp__button--close" title="Close (Esc)"></button> \
	 <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> \
	 <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button> \
	 <div class="pswp__preloader"> \
	 <div class="pswp__preloader__icn"> \
	 <div class="pswp__preloader__cut"> \
	 <div class="pswp__preloader__donut"></div> \
	 </div> \
	 </div> \
	 </div> \
	 </div>\
	 <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"> \
	 <div class="pswp__share-tooltip"> \
	 <a href="#" class="pswp__share--facebook"></a> \
	 <a href="#" class="pswp__share--twitter"></a> \
	 <a href="#" class="pswp__share--pinterest"></a> \
	 <a href="#" download class="pswp__share--download"></a>\
	 </div>\
	 </div>\
	 <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> \
	 <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button> \
	 <div class="pswp__caption"> \
	 <div class="pswp__caption__center"></div> \
	 </div> \
	 </div> \
	 </div> \
	 </div>';

	 /**
	  * Init PhotoSwite
	  */
	  createDialogElement();
	  var pswpElement = document.querySelectorAll('.pswp')[0];
	  var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, defaults );
	  gallery.init();
	}
})(jQuery);